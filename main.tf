module "web" {
  source           = "github.com/MEHERWAN04/terraform-provisioner.git"
  region           = "us-east-2"
  ami              = "ami-024e6efaf93d85776"
  instance_type    = "t2.micro"
  key_name         = "aws-ohio"
  private_key_path = var.private_key_path

}